package es.manelcc.actionbar;

import android.animation.ValueAnimator;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private ImageView imgLogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //referenciamos el ImageView
        imgLogo = (ImageView) findViewById(R.id.imgLogo);
        //Establecemos como Background el recurso de animación
        imgLogo.setBackgroundResource(R.anim.movimiento_cola);
        //Casteamos la Animación
        final AnimationDrawable animColaGato = (AnimationDrawable) imgLogo.getBackground();
        //Generamos en un hilo la animación para no cargar el hilo principal
        imgLogo.post(new Runnable() {

            @Override
            public void run() {
                if (animColaGato != null) {
                    animColaGato.start();
                }
            }
        });


        final TextView tiempo = (TextView) findViewById(R.id.tiempo);

        // Definimos el animator que cambia el valor entero de 10 a 200
        ValueAnimator animacion = ValueAnimator.ofInt(10, 200);
        // Indicamos la duración de la animación
        animacion.setDuration(5000);
        // Establecemos el listener para detectar el cambio del valor entero
        animacion.addUpdateListener(
                new ValueAnimator.AnimatorUpdateListener() {
                    public void onAnimationUpdate(ValueAnimator animation) {
                        int valor = (int) animation.getAnimatedValue();

                        tiempo.setText(String.valueOf(animation.getAnimatedValue()));
                        tiempo.setTextSize(25f);
                        // Durante 5 segundos la variable valor va cambiando
                        // desde 10 hasta 200. Por defecto, esta clase
                        // modifica el valor cada 10 milisegundos
                    }
                }
        );

        // Iniciamos la animación
        animacion.start();


        Button btn = (Button) findViewById(R.id.btn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent ir = new Intent(MainActivity.this, ScrollingActivity.class);
                startActivity(ir);
                overridePendingTransition(R.anim.zoom_back_in, R.anim.zoom_back_out);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case R.id.action_settings:
                Toast.makeText(this, "HAS PULSADO CONFIGURACION", Toast.LENGTH_LONG).show();
                break;
            case R.id.action_delete:
                Toast.makeText(this, "A CONTINUACION VAS A BORRAR, SEGURO", Toast.LENGTH_LONG).show();
                break;
            case R.id.action_save:
                Toast.makeText(this, "A CONTINUACION VAS A GUARDAR, SEGURO", Toast.LENGTH_LONG).show();
                break;
        }
        return true;
    }
}
