package es.manelcc.actionbar;

import android.animation.ObjectAnimator;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

public class ScrollingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolling);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ImageView imgLogo = (ImageView) findViewById(R.id.imgLogo);

        imgLogo.setBackgroundResource(R.anim.movimiento_cola);
        final AnimationDrawable animColaGato = (AnimationDrawable) imgLogo.getBackground();
        imgLogo.post(new Runnable() {

            @Override
            public void run() {
                if (animColaGato != null) {

                    animColaGato.start();
                }

            }
        });
        //Definimos el ObjectAnimator
        ObjectAnimator alfa = ObjectAnimator.ofFloat(imgLogo, "alpha", 0, 1, 1, 1);
        ObjectAnimator beta = ObjectAnimator.ofFloat(imgLogo, "scaleX", 0.3f, 1.05f, 0.9f, 1);
        ObjectAnimator gama = ObjectAnimator.ofFloat(imgLogo, "scaleY", 0.3f, 1.05f, 0.9f, 1);
        //Establecemos una duración
        alfa.setDuration(2500);
        beta.setDuration(4000);
        gama.setDuration(4000);
        //Establecemos la repetición
        alfa.setRepeatCount(ObjectAnimator.INFINITE);
        beta.setRepeatCount(ObjectAnimator.INFINITE);
        gama.setRepeatCount(ObjectAnimator.INFINITE);
        //iniciamos la animación
        alfa.start();
        beta.start();
        gama.start();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }
}
